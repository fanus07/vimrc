# **Installation** #
* Install/extract vim
      * for Windows download [here](https://tuxproject.de/projects/vim/) then download latest lua.dll and put in same directory as vim.exe
* Install vim-plug - https://github.com/junegunn/vim-plug
* Replace content of "~/.vimrc" with contents of "get" file - https://bitbucket.org/fanus07/vimrc/raw/master/get
* Once you run vim, the "global" file will be downloaded and used going forward
* Type and excute the following two commands if you know the plugin list was updated.
      1. **:PlugClean**
      1. **:PlugInstall**
* You can choose not to download the global file every time you run vim by commenting out the curl line in the local .vimrc/_vimrc file

# **Plugin Requirements** #
* python2
* lua
* mono (linux)
* cmake (linux)

# **Building Plugins** #
**Linux**
```
#!cmd
cd ~/.vim/plugged/omnisharp-vim
git submodule update --init --recursive
cd server
xbuild
```
**Windows**
```
#!cmd

cd %HOMEPATH%\.vim\plugged\omnisharp-vim
git submodule update --init --recursive
cd server
msbuild
```